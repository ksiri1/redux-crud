import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../state/action'
import { bindActionCreators } from 'redux'


class PostForm extends Component {
    //TESTT
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            message: ''
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const title = this.state.title;
        const message = this.state.message;
        const post = {
            id: new Date(),
            title,
            message,
            edit: false
        }
        
        this.props.addPost(post)
        console.log(post)
    }

    changeHandler = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div className='post-container'>
                <h1 className='post-heading'>Create Post</h1>
                <form onSubmit={this.handleSubmit}>
                    <input
                        type="text"
                        placeholder="Enter Post Title"
                        name='title'
                        onChange={this.changeHandler}
                    /><br />
                    <textarea
                        required row="5" cols="28" placeholder="Enter Post"
                        name='message'
                        onChange={this.changeHandler}
                        ></textarea><br />
                    <button>Post</button>
                </form>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return bindActionCreators(Object.assign({}, actions), dispatch)
}


export default connect(null, mapDispatchToProps)(PostForm)
