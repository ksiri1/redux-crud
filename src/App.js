import React, { Component } from 'react';
import AllPost from './components/AllPost'
import PostForm from './components/PostForm'


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="navbar">
          <h2>REDUX CRUD</h2>
        </div>
        <div className="flex-container">
          <PostForm></PostForm>
          <AllPost></AllPost>

        </div>

      </div>
    );
  }
}
export default App;