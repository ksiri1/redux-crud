
const postReducer = (state = [], action) => {
    switch(action.type) {
        case 'ADD_POST':
            return state.concat([action.post])
        case 'DELETE_POST':
            return state.filter(post => post.id !== action.id )
        case 'EDIT_POST':
            // return state.map(post => 
            //     post.id === action.id ? {...post, edit: !post.edit}: post
            // )

            // return state.map(post => 
            //     post.id === action.id ? Object.assign({}, state, {
            //         edit: !post.edit
            //     }): post
            // )

            return state.map(post => 
                post.id === action.id ? Object.assign({}, post, {
                    edit: !post.edit
                }): post
            )
        case 'UPDATE_POST':
            console.log(action.data)
            return state.map(post => 
                post.id === action.id ? {
                    ...post,
                    title: action.data.title,
                    message: action.data.message,
                    edit: !post.edit
                } : post
            )
        default: 
            return state;
    }
}   

export default postReducer;