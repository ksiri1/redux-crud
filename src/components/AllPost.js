import React, { Component } from 'react'
import { connect } from 'react-redux';

// Components
import Post from './Post'
import Edit from './Edit'


class AllPost extends Component {

    allPosts = () => {
        const posts = this.props.posts
        return posts && posts.map(post => {
            return post.edit ? <Edit key={post.id} post={post}></Edit> : 
            <Post key={post.id} post={post}></Post>   
        })
    }

    render() {
        return (
            <div className='flex'>
                <h1 className='post-heading'>All Posts</h1>
                { this.allPosts() }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        posts: state
    }
}

export default connect(mapStateToProps)(AllPost)