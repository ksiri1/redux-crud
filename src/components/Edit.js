import React, { Component } from 'react'
import { connect } from 'react-redux';


import * as actions from '../state/action'
import { bindActionCreators } from 'redux'

class Edit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            message: ''
        }
    }

    changeHandler = e => {
        this.setState({
            // [e.target.name]: e.target.value
            title: this.input.value,
            message: this.textarea.value
        });
    }

    updatePost = () => {
        // console.log(this.state)
        console.log({...this.state})
        this.props.updatePost(this.props.post.id, {...this.state})
    }

    cancelPost = () => {
        this.props.editPost(this.props.post.id)
    }


    
    render() {
        return (
            <div className='post-container'>
                 <form >
                    <h2>Edit Post</h2>
                    <input
                        ref={ip => this.input = ip}
                        type="text"
                        name='title'
                        onChange={this.changeHandler}
                        defaultValue={this.props.post.title}
                    /><br />
                    <textarea
                        ref={ta => this.textarea = ta}
                        required row="5" cols="28"
                        name='message'
                        onChange={this.changeHandler}
                        defaultValue={this.props.post.message}
                        ></textarea><br />
                    <div className="button-list">
                    <button onClick={this.updatePost}>Confirm</button>
                    <button onClick={this.cancelPost}>Cancel</button>

                    </div>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Object.assign({}, actions), dispatch)
}

export default connect(null, mapDispatchToProps)(Edit)
