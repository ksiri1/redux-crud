import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../state/action'
import { bindActionCreators } from 'redux'

class Post extends Component {

    deletePost = () => {
        console.log(this.props.post.id)
        this.props.deletePost(this.props.post.id)
    }

    editPost = () => {
        this.props.editPost(this.props.post.id)
        console.log(this.props.post)
    }

    render() {
        return (
            <div className='post-container'>
                <div className="post-content">
                    <h2>{this.props.post.title}</h2>
                    <p>{this.props.post.message}</p>

                </div>                <div className='button-list'>
                    <button onClick={this.editPost}>Edit</button>
                    <button onClick={this.deletePost}>Delete</button>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Object.assign({}, actions), dispatch)
}

export default connect(null, mapDispatchToProps)(Post);