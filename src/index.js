import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import { Provider } from 'react-redux'
import { createStore } from 'redux'
import postReducer from './state/reducer'

import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(postReducer, composeWithDevTools());

ReactDOM.render(
    <Provider store={store}>
        <App></App>
    </Provider>
    , document.getElementById('root'));

